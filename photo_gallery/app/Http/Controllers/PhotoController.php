<?php

namespace App\Http\Controllers;

use App\Models\Album;
use App\Models\Photo;
use App\Models\User;
use Illuminate\Http\Request;
use mysql_xdevapi\Table;

class PhotoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Album $album)
    {
        return view('photos.create' ,['album' => $album]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,Album $album )
    {
        $this ->validatePhoto();

        $filenameWithExtension = $request->file('image')->getClientOriginalName();
        $filename = pathinfo( $filenameWithExtension,PATHINFO_FILENAME);
        $extension =$request->file('image')->getClientOriginalExtension();
        $filenameToStore = $filename. '_' . time() . '.' . $extension;

        $request->file('image')->storeAs('public/album_image', $filenameToStore);
        $photo=  new Photo(request(['name','image']));
        $photo->image = $filenameToStore;
        $photo->album_id  = $album->id;
        $photo->save();


        return redirect('/albums/'.$photo->album_id);

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Photo  $photo
     * @return \Illuminate\Http\Response
     */
    public function show(Photo $photo)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Photo  $photo
     * @return \Illuminate\Http\Response
     */
    public function edit(Photo $photo)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Photo  $photo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Photo $photo)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Photo  $photo
     * @return \Illuminate\Http\Response
     */
    public function destroy(Photo $photo)
    {




        if(auth()->user()->id ==  $photo->albums->user_id)
        {
            $photo->delete();

        }
        return redirect('/albums/'.$photo->album_id);

    }
    public function validatePhoto()
    {
        return request()->validate([
            'name' => ['required','max:20'],
            'image'=>'required|image',
        ]);
    }
}
