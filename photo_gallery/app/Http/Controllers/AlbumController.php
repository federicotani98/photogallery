<?php

namespace App\Http\Controllers;

use App\Models\Album;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AlbumController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $albums = Album::latest()->get();

        return view('home', ['albums' => $albums]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('albums.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
        $this ->validateAlbum();

        $filenameWithExtension = $request->file('cover_image')->getClientOriginalName();
        $filename = pathinfo( $filenameWithExtension,PATHINFO_FILENAME);
        $extension =$request->file('cover_image')->getClientOriginalExtension();
        $filenameToStore = $filename. '_' . time() . '.' . $extension;

        $request->file('cover_image')->storeAs('public/album_covers', $filenameToStore);
        $album=  new Album(request(['name','description','cover_image']));
        $album->cover_image = $filenameToStore;
        $album->user_id  = auth()->user()->id;
        $album->save();



        return redirect('/albums/');



    }


    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Album  $album
     * @return \Illuminate\Http\Response
     */
    public function show(Album $album)
    {
        return  view('albums.show', ['album' => $album]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Album  $album
     * @return \Illuminate\Http\Response
     */
    public function edit(Album $album)
    {

        $this->authorize('edit', $album);
        return view('albums.edit', compact('album'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Album  $album
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Album $album)
    {

        //$this ->validateAlbum();
        //$album->upddate(request(['name','description','cover_image']));
        $album->name = $request->input('name');
        $album->description = $request->input('description');

        if($request->hasFile('cover_image')) {
            $filenameWithExtension = $request->file('cover_image')->getClientOriginalName();
            $filename = pathinfo($filenameWithExtension, PATHINFO_FILENAME);
            $extension = $request->file('cover_image')->getClientOriginalExtension();
            $filenameToStore = $filename . '_' . time() . '.' . $extension;

            $request->file('cover_image')->storeAs('public/album_covers', $filenameToStore);


            $album->cover_image = $filenameToStore;
        }
            $album->user_id = auth()->user()->id;

        $album->save();

        return redirect(auth()->user()->path());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Album  $album
     * @return \Illuminate\Http\Response
     */
    public function destroy(Album $album)
    {
        //dd();
        $this->authorize('destroy', $album);
        $album->delete();
        return redirect(auth()->user()->path());

    }

    public function validateAlbum()
    {
        return request()->validate([
            'name' => ['required','max:20'],
            'cover_image'=>'required|image',
        ]);
    }
}
