<?php

namespace App\Policies;

use App\Models\Album;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class AlbumPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
    public function edit(User $currentuser, Album $album)
    {
        return $currentuser->id === $album->user_id;
    }

    public function destroy(user $currentuser, Album $album)
    {
        return $currentuser->id === $album->user_id;
    }
}
