<?php

namespace App\Policies;

use App\Models\User;
use App\Model\Album;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function edit(User $currentuser, Album $album)
    {
        return $currentuser->id === $album->user_id;
    }

    public function destroy(User $currentuser, User $user)
    {
        return $currentuser->is($user);
    }

}
