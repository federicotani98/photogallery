@extends('layouts.app')
@section('content')

    <div class="ml-12">
        <p class="text-sm">
            @if (auth()->check())

                <a href="/albums/create" class="bg-blue-500 rounded-full border border-gray-300 py-2 px-4 text-black text-xs mr-2">Add photo </a>

    </div>

    @endif
    <div id="wrapper">
        <div id="page" class="container">
            <div id="content">
                <div class="title">

                    <h2>Album :    <p class="font-bold" > {{ $album->name}}</p></h2>
                </div>
                <div>
                    <div class="user--info">
                    <img
                        src="/storage/album_covers/{{$album->cover_image}}"
                        alt=""
                        class = "rounded-full mr-2"
                        width="150"
                        height="200"
>

                    Descrizione:
                {{ $album->description}}

                <p style ="margin-top : 1em "  >

                </p>
            </div>
        </div>
    </div>

@endsection
