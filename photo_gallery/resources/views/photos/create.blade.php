@extends('layouts.app')

@section('content')
    <h1 class="font-bold text-lg mb-4 block">Add Photo</h1>
    <div class="container mx-auto px-6 py-4 bg-gray-400">



<form  method="POST" action="{{ route('photo-store' , $album)  }}" enctype="multipart/form-data">
    @csrf
    <div class="form-group">
        <label for="name">Name</label>
        <div class="control">
            <input class="input @error('name') is-danger @enderror" type="text" name="name" id="name" value="{{old('name')}}" placeholder="Enter name">
            @error('name')
            <p class="text-red-500 text-sm mt-2">{{ $errors->first('name') }}</p>
            @enderror

        </div>
    </div>



    <div class="form-group">
        <label for="cover_image"> image</label>
        <input class="input @error('image') is-danger @enderror" type="file" name="image" id="image" value="{{old('image')}}">
        @error('image')
        <p class="text-red-500 text-sm mt-2">{{ $errors->first('image') }}</p>
        @enderror

    </div>
    <button type="submit" class="px-6 py-3 rounded text-sn uppercase bg-blue-600 text-white">Crea</button>
</form>

    </div>


@endsection














