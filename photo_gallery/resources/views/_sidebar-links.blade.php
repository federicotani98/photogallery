<ul>
    <li>
        <a href="{{route('home')}}" class="font-bold text-lg mb-4 block">
            Home
        </a>
    </li>
    <li>

        <a href="{{auth()->user()->path()}}" class="font-bold text-lg mb-4 block">
            Profile
        </a>

    </li>
    <li>
        <div>
            <p class="text-sm">
                @if (auth()->check())

                    <a href="/albums/create" class="font-bold text-lg mb-4 block" >Create Album</a>

        </div>

        @endif
    </li>

    <li>

        <a href="#" class="font-bold text-lg mb-4 block">
            @if (auth()->check())

                <a href="/logout" class="font-bold text-lg mb-4 block" >Logout</a>
            @endif
        </a>
    </li>
</ul>

