@extends('layouts.app')


@section('content')
   <h1 class="font-bold text-lg mb-4 block">Dashboard</h1>
   <br>
    <div>
        <div id="wrapper">
            <div id="page" class="container">

                @forelse ($albums as $album)
                    <br>
                    <br>
                    <a  class="font-bold" href="{{route('albums.show', $album)}}">  {{ $album->name }} </a>
<br>
                    <div>
                        <img
                            src="/storage/album_covers/{{$album->cover_image}}"
                            alt=""
                            width="400"
                            height="400"
                        >


                        <p class="text-sm">created {{ $album->created_at->diffForHumans()}}</p>

                        by: <a href="{{route('profile', $album->user)}}">  {{$album->user->name}}</a>
                    </div>
<br>
                @empty
                    <br>
                    <br>

                    <p>Nessun album , sii il primo ad inserirne uno!</p>


                @endforelse

            </div>
        </div>

    </div>


@endsection


