@extends('layouts.app')

@section('content')

    @php $albums= App\Models\Album::all()->sortByDesc('id'); @endphp

    <header class="mb-6">



        <div class="flex justify-between items-center mb-6">
            <div>
                @if(auth()->user()->is($user))
                    <h2 class="font-bold text-2xl mb-2">Il mio profilo</h2>
                @else
                    <h2 class="font-bold text-2xl mb-2">Profilo di {{$user->name}}</h2>
                @endif
                <p class="text-sm">Joined {{ $user->created_at->diffForHumans()}}</p>
            </div>
            <br>

            <br>
            <br>
            <br>
            <br>
            <br>

                <div class="flex">



                </div>

            </div>

        <div>
            <div id="wrapper">
                <div id="page" class="container">

                    @foreach ($albums as $album)
                        @if( $user->id == $album->user->id)
                            <br>
                            <br>
                            <a  class="font-bold" href="{{route('albums.show', $album)}}">  {{ $album->name }} </a>
                            <br>
                            <div>
                                <img
                                    src="/storage/album_covers/{{$album->cover_image}}"
                                    alt=""
                                    width="400"
                                    height="400"
                                >


                                <p class="text-sm">created {{ $album->created_at->diffForHumans()}}</p>

                                by: <a href="{{route('profile', $album->user)}}">  {{$album->user->name}}</a>
                            </div>
                            <br>

                        @endif
                    @endforeach

                </div>
            </div>




@endsection
