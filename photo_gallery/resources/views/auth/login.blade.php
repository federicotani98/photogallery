@extends('layouts.app')

@section('content')

    <div class="container mx-auto px-6 py-4 bg-gray-400" >
        <div class="container">
                    <div>{{ __('Login') }}</div>
                    <br>

                    <div >
                        <form method="POST" action="{{ route('login') }}">
                            @csrf


                                <label for="email" class="form-label">{{ __('E-Mail Address') }}</label>


                                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                    @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror

<br>
                                <label for="password" class="">{{ __('Password') }}</label>


                                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                    @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror


                            <div class="form-group row">
                                <div>
                                    <br>
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                        <label class="form-check-label" for="remember">
                                            {{ __('Remember Me') }}
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div>
                                <div class="col-md-8 form-group row" >
                                    <button type="submit" class="px-6 py-3 rounded text-sn uppercase bg-blue-600 text-white">
                                        {{ __('Login') }}
                                    </button>

                                    @if (Route::has('password.request'))
                                        <a class="btn btn-link" href="{{ route('password.request') }}">
                                            {{ __('Forgot Your Password?') }}
                                        </a>
                                    @endif
                                </div>
                            </div>
                        </form>
                    </div>

        </div>
    </div>
@endsection
