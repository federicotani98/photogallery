@extends('layouts.app')
@section('content')




    <br>
    <br>
    <div id="wrapper">
        <div class="d-flex mb-5">

            @if (auth()->check())
            @if(\Illuminate\Support\Facades\Auth::User()->id==$album->user_id)
                <a href="/photos/create/{{$album->id}}" class="bg-blue-500 rounded-full border border-gray-300 py-2 px-4 text-black text-xs mr-2">Add photo </a>
                <a href="/albums/{{$album->id}}/edit" class="bg-yellow-500 rounded-full border border-gray-300 py-2 px-4 text-black text-xs mr-2">Edit album </a>
                <a href="/albums/{{$album->id}}/delete" class="bg-red-600 rounded-full border border-gray-300 py-2 px-4 text-black text-xs mr-2">Delete album</a>
        </div>@endif
        @endif
        <div id="page" class="container">
            <div id="content">
                <div class="title">

                    <h2> Album name: <p class="font-bold" >  {{ $album->name}}</p></h2>
                </div>
                <div>
                    <br>
                    <div class="user--info">

                        <i>{{ $album->description}} </i>

                        <div style ="margin-top : 1em "  >


                        </div>
                    </div>
                </div>


                <div id="myCarousel" class="carousel slide" data-ride="carousel">
                    <ol class="carousel-indicators">
                        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                        <li data-target="#myCarousel" data-slide-to="1"></li>
                        <li data-target="#myCarousel" data-slide-to="2"></li>
                    </ol>

                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <svg class="bd-placeholder-img" width="0%" height="0%" xmlns="http://www.w3.org/2000/svg" role="img" aria-label=" :  " preserveAspectRatio="xMidYMid slice" focusable="false"><title> </title><rect width="100%" height="100%" fill="#777"/><text x="50%" y="50%" fill="#777" dy=".3em"> </text></svg>
                            <img class="d-block w-100"  src="/storage/album_covers/{{$album->cover_image}}"/>


                        </div>

                        @foreach($album->photos as $photo)
                            <div class="carousel-item  ">

                                <img class="d-block w-100" src="/storage/album_image/{{$photo->image}}"/>

                            </div>


                        @endforeach

                    </div>
                    <a class="carousel-control-prev" href="#myCarousel" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#myCarousel" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
                <div>
                    <div id="wrapper">
                        <div id="page" class="container">

                            @foreach ($album->photos as $photo)
                                <br>
                                <br>
                                <a  class="font-bold">  {{ $photo->name }} </a>
                                <br>
                                <div>
                                    <img
                                        src="/storage/album_image/{{$photo->image}}"
                                        alt=""
                                        width="400"
                                        height="400"
                                    >


                                </div>
                                <br>
                                @if(auth()->check() &&     \Illuminate\Support\Facades\Auth::User()->id==$album->user_id)

                                <a href="/photos/{{$photo->id}}/delete" class="bg-red-600 rounded-full border border-gray-300 py-2 px-4 text-black text-xs mr-2">Delete photo</a>
                                @endif


                            @endforeach

                        </div>
                    </div>


                    <div class="container">


                        <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                            <div class="carousel-inner">

                                @foreach ($album->photos as $photo)
                                    <div class="carousel-item  ">

                                        <img class="d-block w-100" src="/storage/album_image/{{$photo->image}}"/>

                                        @if (auth()->check())
                                            <a href="/photos/{{$photo->id}}/delete" class="bg-red-600 rounded-full border border-gray-300 py-2 px-4 text-black text-xs mr-2">Delete photo</a>
                                        @endif

                                    </div>
                                @endforeach


                                <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Previous</span>
                                </a>
                                <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Next</span>
                                </a>
                            </div>


                        </div>
                    </div>
                </div>

            </div>
        </div>


@endsection


