@extends('layouts.app')

@section('content')
    <h1 class="font-bold text-lg mb-4 block">Edit Album</h1>


    <div class="container mx-auto px-6 py-4 bg-gray-400">

        <form  method="POST" action="{{ route('album-update', $album->id) }}" enctype="multipart/form-data">
            @csrf
            @method("PUT")
            <div class="form-group">
                <label for="name">Name</label>
                <div class="control">
                    <input class="input" type="text" name="name" id="name" value="{{$album->name}}">

                </div>
            </div>

            <div class="form-group">
                <label for="description" class="w-full">Description</label>
                <div class="control">
                <input class="input" type="textarea" name="description" id="description" value="{{$album->description}}">
                </div>
            </div>


            <div class="form-group">
                <label for="cover_image">Cover image</label>
                <img
                    src="/storage/album_covers/{{$album->cover_image}}"
                    alt=""
                    width="400"
                    height="400"
                >
                <input class="input " type="file" name="cover_image" id="cover_image" value="{{$album->cover_image}}">


            </div>
            <button type="submit" class="px-6 py-3 rounded text-sn uppercase bg-blue-600 text-white">Salva Modifiche</button>
        </form>

    </div>

@endsection
