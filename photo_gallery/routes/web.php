<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::middleware('auth')->group(function () {
    //Route::resource("albums", "App\Http\Controllers\AlbumController");
    Route::get('/albums/create', 'App\Http\Controllers\AlbumController@create');
    Route::post('/albums/store', 'App\Http\Controllers\AlbumController@store')->name('album-store');
    Route::get('/albums/{album}/edit', 'App\Http\Controllers\AlbumController@edit');
    Route::get('/albums/{album}/delete', 'App\Http\Controllers\AlbumController@destroy');

    Route::put('/albums/{album}', 'App\Http\Controllers\AlbumController@update')->name('album-update');

    Route::get('/photos/create/{album}', 'App\Http\Controllers\PhotoController@create');
    Route::post('/photos/store/{album}', 'App\Http\Controllers\PhotoController@store')->name('photo-store');
    Route::get('/photos/{photo}/delete', 'App\Http\Controllers\PhotoController@destroy');

    Route::get('/albums', 'App\Http\Controllers\AlbumController@index')->name('home');
    Route::get('/profiles/{user:name}', 'App\Http\Controllers\ProfilesController@show')->name('profile');
    Route::get('/albums/{album}', 'App\Http\Controllers\AlbumController@show')->name('albums.show');


});
Auth::routes();

//Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('logout', function ()
{
    auth()->logout();
    Session()->flush();

    return Redirect::to('/');
})->name('logout');
